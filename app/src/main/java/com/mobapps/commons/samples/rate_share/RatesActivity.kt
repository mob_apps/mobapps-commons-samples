package com.mobapps.commons.samples.rate_share

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobapps.commons.rate.helpers.askIfLiked
import com.mobapps.commons.rate.helpers.rateApp
import com.mobapps.commons.samples.R
import kotlinx.android.synthetic.main.activity_rates.*

class RatesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rates)

        btRateApp.setOnClickListener {
            rateApp()
        }

        btAskIfLiked.setOnClickListener {
            askIfLiked()
        }

        btAskIfLikedThenFinish.setOnClickListener {
            askIfLiked(true)
        }
    }
}
