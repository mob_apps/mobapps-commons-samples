package com.mobapps.commons.samples

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mobapps.commons.samples.rate_share.RatesActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btRates.setOnClickListener {
            startActivity(Intent(this, RatesActivity::class.java))
        }
    }
}
